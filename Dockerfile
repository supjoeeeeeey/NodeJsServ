FROM alpine:latest
MAINTAINER SupJoey
ADD . /app/
WORKDIR /app
RUN apk update && \
apk add nodejs npm yarn && \
yarn install
EXPOSE 443
CMD ["npm", "start"]