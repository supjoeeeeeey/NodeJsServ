const express = require('express')
const https = require('https')
const fs = require('fs')
const app = express()
const http = require('http')
const port = 443
const httpOpts = {
  cert: fs.readFileSync('./ssl/supjoey.crt'),
  key: fs.readFileSync('./ssl/supjoey.key')
}



app.use('/public', express.static('public'));

app.get('/weather', function(req, res){
  console.log(req.query);
  var location = req.query.location;
  http.get(encodeURI(`http://api.k780.com?app=weather.today&weaid=${location}&appkey=34189&sign=00a9dc0ce3a80506ee73f982df8b59db&format=json`)
    , (response) => {
      var bodyChunks = []
      response.on('data', function(chunk) {
        bodyChunks.push(chunk)
      }).on('end', function() {
        var body = Buffer.concat(bodyChunks)
        res.format({
          'text/html': function() {
            res.send(body)
          }
        })
      })
  })
});

app.get('/futureweather', function(req, res){
  console.log(req.query);
  var location = req.query.location;
  http.get(encodeURI(`http://api.k780.com?app=weather.future&weaid=${location}&appkey=34189&sign=00a9dc0ce3a80506ee73f982df8b59db&format=json`)
    , (response) => {
      var bodyChunks = []
      response.on('data', function(chunk) {
        bodyChunks.push(chunk)
      }).on('end', function() {
        var body = Buffer.concat(bodyChunks)
        res.format({
          'text/html': function() {
            res.send(body)
          }
        })
      })
  })
});

app.get('/healthcheck', (req, res) => {
  res.send(200, 'OK');
});

https.createServer(httpOpts, app)
  .listen(port, function(){
    console.log(`Successfully connected to localhost:${port}!`)
  });