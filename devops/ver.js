const { getTag, getCommits, updateVersion } = require('./git.js');
const fs = require('fs-extra');
const fixRegx = new RegExp(`\\bfix\\b`, 'i');
const bkRegx = new RegExp(`\\brefactor\\b`);

async function generateSemVer(){
    const result = await getTag();
    if(result.commit != null){
        const commits = await getCommits(result.commit['created_at']);
        let commitMessage = [];
        let version = getVersion(commits, result.version.split('.'), commitMessage);
        let message = commitMessage.map( (ele, index) => `${index+1}. ${ele}` ).join('\r\n');
        let note = {
            version: version,
            commits: message
        };
        await updateVersion(note);
        fs.writeJSONSync('version.json', note);
    }
}

function getVersion(commits, currVersion, commitMessage){
    for(let i = 0; i < currVersion.length; i++){
        currVersion[i] = parseInt(currVersion[i]);
    }
    currVersion[0] = parseInt(currVersion[0]);
    commits.forEach(element => {
        checkVersionLevel(element, currVersion);
        commitMessage.push(element.message);
    });
    let version = `v${currVersion.join('.')}`;
    return version;
}

function checkVersionLevel(commit, currVersion){    
    if(bkRegx.test(commit.message)){
        // breaking change
        currVersion[0] = currVersion[0] + 1; 
    }
    else if(fixRegx.test(commit.message)){
        // fix
        currVersion[1] = currVersion[1] + 1; 
    }
    else{
        // patch
        currVersion[2] = currVersion[2] + 1;
    }    
}

generateSemVer();