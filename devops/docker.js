const { exec } = require('child_process');
const ver = require('../version.json');

function buildDocker(){
    exec(`docker build -t ccr.ccs.tencentyun.com/supjoey/node_server:${ver.version} .`, (err, stdout, stderr) => {
      exec(`docker push ccr.ccs.tencentyun.com/supjoey/node_server:${ver.version}`, (err, stdout, stderr) => {
        console.log(`err: ${err}`);
        console.log(`stdout: ${stdout}`);
        console.log(`PUSH ${ver.version} SUCCEED!!!`);
      });
    });
}

buildDocker();