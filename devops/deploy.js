const { exec } = require('child_process');
const ver = require('../version.json');
const axios = require('axios');
const endpoint = process.env['CLOUD_HOST'];
const deployToken = process.env['DEPLOY_TOKEN'];

async function deployDocker(){
    let imageName = `ccr.ccs.tencentyun.com/supjoey/node_server:${ver.version}`;
    let resp = await axios.get(`${endpoint}/deploy?token=${deployToken}&image=${imageName}`);
    console.log(resp);
    console.log(`DEPLOY DOCKER IMAGE - ${imageName} SUCCEED!`);
}

deployDocker();