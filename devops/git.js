const axios = require('axios');
const gitlab_endpoint = 'https://gitlab.com/api/v4/projects';
const projectId = process.env['PROJECT_ID'];
const accessToken = process.env['ACCESS_TOKEN'];

async function getTag(){
    try{
        const resp = await axios.get(
            `${gitlab_endpoint}/${projectId}/repository/tags`, 
            {
                headers:{
                    "Private-Token": accessToken
                }
            });
        if(resp.data && resp.data.length == 0){
            return {
                version: '0.0.1',
                commit: null
            };
        }
        let version = resp.data[0].name.split('v');
        return {
            version: version[version.length - 1],
            commit: resp.data[0].commit
        };    
    }catch(err){
        console.log(JSON.stringify(err));
    }
}

async function getCommits(createDate){
    const resp = await axios.get(`${gitlab_endpoint}/${projectId}/repository/commits?since=${createDate}`, {
        headers:{
            "Private-Token": accessToken
        }
    });
    return resp.data;
}

async function updateVersion(note){
    const resp = await axios.post(
        `${gitlab_endpoint}/${projectId}/repository/tags`,
        {
            tag_name: note.version,
            ref: 'master',
            release_description: note.commits
        },{
            headers:{
                "Private-Token": accessToken
            }
        }
    );
}

module.exports = {
    getTag,
    getCommits,
    updateVersion
}